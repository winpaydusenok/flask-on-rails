from app import db

post_category = db.Table('post_category',
                         db.Column('post_id', db.Integer, db.ForeignKey('post.id')),
                         db.Column('category_id', db.Integer, db.ForeignKey('category.id'))
                         )
