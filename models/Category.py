from app import db
from models.post_category import *


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    # post = db.relationship('Post',
    #     #backref=db.backref('posts', lazy=True),
    #     secondary=association_table
    #     )
    post = db.relationship('Post',
                           backref=db.backref('categories', lazy=True),
                           lazy='subquery',
                           secondary=post_category
                           )

    def __repr__(self):
        return '<Category %r>' % self.name
