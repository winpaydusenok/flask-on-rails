from flask import jsonify


def render_json(query_result):
    if type(query_result) is list:
        res = []
        for el in query_result:
            atom = {}
            cols = list(el.__table__.columns)
            for c in cols:
                atom[c.name] =  getattr(el, c.name)
            res.append(atom)
        return jsonify(res)
    elif hasattr(query_result,'__table__'):
        res = {}
        cols = list(query_result.__table__.columns)
        for c in cols:
            res[c.name] =  getattr(query_result, c.name)
        return jsonify(res)
    return "Error object query"