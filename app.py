from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_security import SQLAlchemyUserDatastore, Security
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from config import *
import os

app = Flask(__name__, template_folder="views")

FLASK_ENV = os.getenv('FLASK_ENV')
if FLASK_ENV == 'development':
    app.config.from_object(DevelopmentConfig)
elif FLASK_ENV == 'production':
    app.config.from_object(ProductionConfig)
elif FLASK_ENV == 'test':
    app.config.from_object(TestingConfig)

db = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = Manager(app)


manager.add_command('db', MigrateCommand)

## Admin
from models.Package import *
from models.Category import *
from models.User import *
from models.Role import *

admin = Admin(app)
admin.add_view(ModelView(Post, db.session))
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Role, db.session))
admin.add_view(ModelView(Category, db.session))

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)
