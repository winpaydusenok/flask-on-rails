from app import app
from controllers import News

app.add_url_rule('/', view_func=News.show, methods=['GET'])
app.add_url_rule('/delete/<id>', view_func=News.delete_post, methods=['GET'])
