from models.Package import *
from models.Category import *
from plugins import render_json
from flask import render_template, redirect, request
from flask_security import login_required
from app import app
import json
import requests

@app.route('/')
@login_required
def show():
    posts = Post.query.all()
    return render_template("index.html", posts=posts)

@app.route('/add', methods=['POST'])
def add():
    title = request.values.get("title")
    body = request.values.get("body")
    post = Post(title=title, body=body)
    db.session.add(post)
    db.session.commit()
    return redirect('/')

@app.route('/delete/<id>')
def delete_post(id):
    post = Post.query.filter_by(id=id).first()
    db.session.delete(post)
    db.session.commit()
    return redirect('/')
    # rr.text #render_json(post)
